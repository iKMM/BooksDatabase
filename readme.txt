This repository does not contain any commits, as this project was uploaded to 
GitLab purely for the purposes of my portfolio.

This project is a PHP and MySQL based 'Book Website'. The original specification
was to create a site that allowed CRUD interface with a database using PHP.

All of the additional styling and the use of the HTML5 boilerplate was chosen
and designed by me. I use Bootstrap 3 in order to help with the styling of this
project, as I wanted a framework that could quickly allow the site to look good
so that I could focus on the actual back-end design.

The PHP code within uses the database definitions of the Universities database,
but this has since been closed and therefore the data isn't sensitive. I have
therefore maintained the original code (which is stored in an insecure way
originally, as this is merely a pet project) as is.

I took extensive measures to ensure that the project was free from any bugs 
I found. I did testing, including testing unexpected things that users might do.
During my testing I found bugs involving most parts of the system, as could
be expected. I made sure to fix these and make them as user-proof as possible.

