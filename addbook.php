<?php
// 130 LAB DETAILS

	$hostname = 'localhost';
	$database = 'userdb147'; // enter your database name here
	$username = 'user147';  // your username here
	$password = 'PX34ZU97ST19'; //  your password here


	/*
		$hostname = 'localhost';
		$database = 'SCC130';
		$username = 'root';
		$password = 'root';
	*/
	$connection = new mysqli($hostname, $username, $password, $database);

	if ($connection->connect_error) die($connection->connect_error);

	/*
	The server side validation. Checks the variables.
	*/
	function validate()
	{
		global $error;
		global $connection;
		$error = "Book successfully added.";

		/*
			Presence checks.
		*/
		if(empty($_POST['author']))
		{
			$error = "No author submitted!";
			return false;
		}
		else if(empty($_POST['title']))
		{
			$error = "No title submitted!";
			return false;
		}
		else if(empty($_POST['genre']))
		{
			$error = "No genre submitted!";
			return false;
		}
		else if(empty($_POST['year']))
		{
			$error = "No year submitted!";
			return false;
		}
		else if(empty($_POST['isbn']))
		{
			$error = "No ISBN submitted!";
			return false;
		}

		/*
			Length checks.
		*/
		if(strlen($_POST['author']) > 50 )
		{
			$error = "Author name too long";
			return false;
		}
		else if(strlen($_POST['title']) > 60 )
		{
			$error = "Author name too long";
			return false;
		}
		else if(strlen($_POST['genre']) > 20 )
		{
			$error = "genre name too long";
			return false;
		}
		else if(strlen($_POST['year']) > 4 )
		{
			$error = "Year too long";
			return false;
		}
		else if(strlen($_POST['isbn']) != 10 && strlen($_POST['isbn']) != 13)
		{
			$error = "Invalid ISBN";
			return false;
		}

		/*
			Type checks on year/isbn.
		*/
		if(!(is_numeric($_POST['year'])))
		{
			$error = "Year not numeric. Please enter in format YYYY";
			return false;
		}
		if(!(is_numeric($_POST['isbn'])))
		{
			$error = "ISBN not numeric. Remove any invalid characters such as dashes.";
			return false;
		}


		/*
			Next few lines check if the book with the ISBN already exists.
			This is to prevent primary key clashes with the database.
		*/
		$isbn = $_POST['isbn'];
		$query = "SELECT isbn FROM books WHERE isbn = " . "'$isbn'";

		$result = $connection->query($query);

		if(mysqli_num_rows($result) > 0)
		{
			$error = "Error: ISBN already found in database.";
			return false;
		}

		return true;
	}

	/*
		The test input function is used 
		to prevent unncessary spaces in input or
		html special characters that might
		have been caught in the filter.
	*/
	function test_input($data) {			
	  $data = trim($data);
	  $data = stripslashes($data);
	  $data = htmlspecialchars($data);
	  return $data;
	}

	/*
		Checks if the server has sent using 'POST'
		if it hasn't (as it wouldn't when you first 
		load the page), then it will skip the process.
	*/
	if ($_SERVER["REQUEST_METHOD"] == "POST") {
	    $author = test_input($_POST['author']);
	    $title = test_input($_POST['title']);
	    $genre = test_input($_POST['genre']);
	    $year = test_input($_POST['year']);
	    $isbn = test_input($_POST['isbn']);

	    $validated = validate();      //Calls validation. Only runs query if validation passes.

	    if($validated)
	    {
	    	$query    = "INSERT INTO books VALUES" .
	    	  "('$author', '$title', '$genre', '$year', '$isbn')";
	    	$result   = $connection->query($query);
	    	if (!$result) echo "INSERT failed: $query<br>" .
	    	  $connection->error . "<br><br>"; 	
	    } 	    
	}

	/*
	Next 4 lines are used to determine the number of books you have stored in the database.
	This is used to display under the title as 'You currently have x books'
	*/
	$queryCount = "SELECT COUNT(isbn) FROM books";

	$resultCount = $connection->query($queryCount);

	$resultCount->data_seek(1);

	$row = $resultCount->fetch_row();

	$connection->close();

?>



<head>
	<title>Add new book</title>
	<link rel="stylesheet" href="css/normalize.css">
    
    <link rel="stylesheet" href="css/bootstrap.min.css">

    <link rel="stylesheet" href="css/main.css">

    <script type="text/javascript" src="js/main.js?1234" ></script>

    <meta name="viewport" content="width=device-width, initial-scale=1">
</head>

<body>
	<nav class="navbar navbar-default">
	  <div class="container-fluid">
	    <div class="navbar-header">
	      <a class="navbar-brand" href="">SCC130 - The Book Collection</a>
	    </div>
	    <ul class="nav navbar-nav">
	      <li><a href="index.html">Home</a></li>
	      <li class="active"><a href="addbook.php">Add new book</a></li>
	      <li><a href="viewbooks.php">View books</a></li>
	      <li><a href="search.php">Search</a></li>
	    </ul>
	  </div>
	</nav>
	<div class="container">
		<div class="page-header">
			<h1 id="pageTitle">Add new book to your collection</h1>
			<p id="numberBooks">You currently have <b><?php echo $row[0]; ?></b> books</p>
		</div>
	</div>

	<br>

	<form name="addbook" class="form-horizontal" action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]);?>" method="post">
  		<div class="form-group">
    		<label class="control-label col-sm-4" name="authorLabel">Author: </label>
    		<div class="col-sm-4">
    			<input type="text" class="form-control" name="author" maxlength="50" placeholder="Enter Author">
    		</div>
  		</div>
  		<div class="form-group">
    		<label class="control-label col-sm-4" name="titleLabel">Title: </label>
    		<div class="col-sm-4">
    			<input type="text" class="form-control" name="title" maxlength="60" placeholder="Enter Book Title">
    		</div>
  		</div>
  		<div class="form-group">
    		<label class="control-label col-sm-4" name="genreLabel">Genre: </label>
    		<div class="col-sm-4">
    			<input type="text" class="form-control" name="genre" maxlength="20" placeholder="Enter Book Genre">
    		</div>
  		</div>
  		<div class="form-group">
    		<label class="control-label col-sm-4" name="yearLabel">Year: </label>
    		<div class="col-sm-4">
    			<input type="text" class="form-control" name="year" maxlength="4" placeholder="Enter Year Published">
    		</div>
  		</div>
  		<div class="form-group">
    		<label class="control-label col-sm-4" name="isbnLabel">ISBN: </label>
    		<div class="col-sm-4">
    			<input type="text" class="form-control" name="isbn" maxlength="13" placeholder="Enter ISBN Number for Book (Without dashes)">
    		</div>
  		</div>
  		<div class="form-group"> 
            <div class="col-sm-offset-4 col-sm-6">
                <button type="submit" class="btn btn-default">Submit</button>
            </div>
        </div>
        <div class="form-group">
        	<label class ="control-label col-sm-6"><?php if(isset($error)) { echo $error; } //Displays error message if error exists.?></p>
        </div>
	</form>
</body>