<?php
// 130 LAB DETAILS

	$hostname = 'localhost';
	$database = 'userdb147'; // enter your database name here
	$username = 'user147';  // your username here
	$password = 'PX34ZU97ST19'; //  your password here
/*
	$hostname = 'localhost';
	$database = 'SCC130';
	$username = 'root';
	$password = 'root';
*/
	/*
	Next 4 lines are used to determine the number of books you have stored in the database.
	This is used to display under the title as 'You currently have x books'
	*/
	$queryCount = "SELECT COUNT(isbn) FROM books";

	$connection = new mysqli($hostname, $username, $password, $database);

	if ($connection->connect_error) die($connection->connect_error);

	$resultCount = $connection->query($queryCount);

	$resultCount->data_seek(1);

	$row = $resultCount->fetch_row();

?>



<head>
	<title>Search books</title>
	<link rel="stylesheet" href="css/normalize.css">
    
    <link rel="stylesheet" href="css/bootstrap.min.css">

    <link rel="stylesheet" href="css/main.css">

    <script type="text/javascript" src="js/main.js?1234" ></script>

    <meta name="viewport" content="width=device-width, initial-scale=1">
</head>

<body>
	<nav class="navbar navbar-default">
	  <div class="container-fluid">
	    <div class="navbar-header">
	      <a class="navbar-brand" href="">SCC130 - The Book Collection</a>
	    </div>
	    <ul class="nav navbar-nav">
	      <li><a href="index.html">Home</a></li>
	      <li><a href="addbook.php">Add new book</a></li>
	      <li><a href="viewbooks.php">View books</a></li>
	      <li class="active"><a href="search.php">Search</a></li>
	    </ul>
	  </div>
	</nav>
	<div class="container">
		<div class="page-header">
			<h1 id="pageTitle">Search your collection</h1>
			<p id="numberBooks">You currently have <b><?php echo $row[0]; ?></b> books</p>
		</div>
	</div>
		<form name="search" class="form-horizontal" action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]);?>" method="post">
			<div class="form-group">
		  		<label class="control-label col-sm-5" name="fieldLabel">Field: </label>
		  		<div class="col-sm-2">
		  			<select class="form-control" name="field">
		  			    <option>Author</option>
		  			    <option>Title</option>
		  			    <option>Genre</option>
		  			    <option>Year</option>
		  			    <option>ISBN</option>
		  			</select>
		  		</div>
			</div>
	  		<div class="form-group">
	    		<label class="control-label col-sm-5" name="searchLabel">Search: </label>
	    		<div class="col-sm-2">
	    			<input type="text" class="form-control" name="search" maxlength="50" placeholder="Enter Search Term">
	    		</div>
	  		</div>
	  		<div class="form-group"> 
	            <div class="col-sm-offset-5 col-sm-6">
	                <button type="submit" name="action" value="update" class="btn btn-default">Search</button>
	            </div>
        	</div>
		</form>
	<?php
		/*
			Checks if the server has sent using 'POST'
			if it hasn't, won't filter search.
		*/
		if ($_SERVER["REQUEST_METHOD"] == "POST") {
			$field = strtolower($_POST['field']);
			$search = $_POST['search'];

			$query = "SELECT author, title, genre, year, isbn FROM books WHERE `$field` LIKE '%$search%' ORDER BY author ASC";
		}
		else
		{
			$query = "SELECT author, title, genre, year, isbn FROM books ORDER BY author ASC";
		}

		$result = $connection->query($query);

		$found = mysqli_num_rows($result);

		echo '<div class="container">';
		echo "<div class='form-group'>
		    		<label class='control-label col-sm-offset-4 col-sm-5' name='searchLabel'>Search returned: $found items</label>
		      </div>";
		echo '<table class="table table-striped">
			    <thead>
			      <tr>
			        <th>Author</th>
			        <th>Title</th>
			        <th>Genre</th>
			        <th>Year</th>
			        <th>ISBN</th>
			        <th></th>
			      </tr>
			    </thead>
			    <tbody>';

		

		if(mysqli_num_rows($result) > 0)
		{
			while($row = $result->fetch_assoc()) {
			        echo "<tr><td>" . $row['author'] . "</td><td>" . $row['title'] . "</td><td>" . $row['genre'] . "</td><td>" . $row['year'] . "</td><td>" . $row['isbn'] . "</td><td>" . "<a href='editbook.php?isbn=" . $row['isbn'] . "'>Edit</a>" . "</td></tr>";
			    }
		}
		echo '</tbody>';
		echo '</table>';
		echo '</div>';

		$connection->close();
		?>
</body>