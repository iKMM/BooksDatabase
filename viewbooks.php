<?php
// 130 LAB DETAILS

	$hostname = 'localhost';
	$database = 'userdb147'; // enter your database name here
	$username = 'user147';  // your username here
	$password = 'PX34ZU97ST19'; //  your password here

	/*
		$hostname = 'localhost';
		$database = 'SCC130';
		$username = 'root';
		$password = 'root';
	*/
	/*
	Next 4 lines are used to determine the number of books you have stored in the database.
	This is used to display under the title as 'You currently have x books'
	*/
	$queryCount = "SELECT COUNT(isbn) FROM books";

	$connection = new mysqli($hostname, $username, $password, $database);

	if ($connection->connect_error) die($connection->connect_error);

	$resultCount = $connection->query($queryCount);

	$resultCount->data_seek(1);

	$row = $resultCount->fetch_row();

?>



<head>
	<title>View your books</title>
	<link rel="stylesheet" href="css/normalize.css">
    
    <link rel="stylesheet" href="css/bootstrap.min.css">

    <link rel="stylesheet" href="css/main.css">

    <script type="text/javascript" src="js/main.js?1234" ></script>

    <meta name="viewport" content="width=device-width, initial-scale=1">
</head>

<body>
	<nav class="navbar navbar-default">
	  <div class="container-fluid">
	    <div class="navbar-header">
	      <a class="navbar-brand" href="">SCC130 - The Book Collection</a>
	    </div>
	    <ul class="nav navbar-nav">
	      <li><a href="index.html">Home</a></li>
	      <li><a href="addbook.php">Add new book</a></li>
	      <li class="active"><a href="viewbooks.php">View books</a></li>
	      <li><a href="search.php">Search</a></li>
	    </ul>
	  </div>
	</nav>
	<div class="container">
		<div class="page-header">
			<h1 id="pageTitle">View your whole collection</h1>
			<p id="numberBooks">You currently have <b><?php echo $row[0]; ?></b> books</p>
		</div>
	</div>
	<?php
		echo '<div class="container">';
		echo '<table class="table table-striped">
			    <thead>
			      <tr>
			        <th>Author</th>
			        <th>Title</th>
			        <th>Genre</th>
			        <th>Year</th>
			        <th>ISBN</th>
			        <th></th>
			      </tr>
			    </thead>
			    <tbody>';

		$query = "SELECT author, title, genre, year, isbn FROM books ORDER BY author ASC";

		$result = $connection->query($query);

		if(mysqli_num_rows($result) > 0)
		{
			while($row = $result->fetch_assoc()) {
			        echo "<tr><td>" . $row['author'] . "</td><td>" . $row['title'] . "</td><td>" . $row['genre'] . "</td><td>" . $row['year'] . "</td><td>" . $row['isbn'] . "</td><td>" . "<a href='editbook.php?isbn=" . $row['isbn'] . "'>Edit</a>" . "</td></tr>";
			    }
		}
		else {
			echo "No books found! :(";
		}
		echo '</tbody>';
		echo '</table>';
		echo '</div>';

		$connection->close();
		?>
</body>