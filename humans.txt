# humanstxt.org/
# The humans responsible & technology colophon

# TEAM

    Matthew Marsden - Developer

# THANKS

    Phil Benachour and the 130 team

# TECHNOLOGY COLOPHON

    CSS3, HTML5
    Apache Server Configs, jQuery, Modernizr, Normalize.css
